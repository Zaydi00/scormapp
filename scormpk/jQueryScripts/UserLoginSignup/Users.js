﻿$(document).ready(function () {
    ClearInputs();
})
//Button Click
$('#btnLogin').click(function (e) {
    e.preventDefault();
    //Inputs Mapper
    var _email = $('#txtemail').val();
    var _password = $('#txtpassword').val();

    //Ajax Call for Sending Mail
    
    if (_email == "") {
        swal({
            title: "Error",
            text: "Please enter your email address",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_password == "") {
        swal({
            title: "Error",
            text: "Please enter your password",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else {
        //Ajax Call for Login Users
        $.ajax({
            type: "POST",
            url: '/LoginSignup/Login/',
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify({email: _email, password: _password }),
            dataType: "json",
            success: function (recData) {
                if (recData.status == "success") {

                    //Message Success
                    swal({
                        title: "Done!",
                        text: recData.data,
                        icon: "success",
                        timer: 1000,
                        showConfirmButton: false
                    });
                    location.reload(true);
                    ClearInputs();
                }
                else {
                    swal({
                        title: "Error!",
                        text: recData.data,
                        icon: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            },
            error: function () {
                swal({
                    title: "Error!",
                    text: recData.data,
                    icon: "error",
                    timer: 1000,
                    showConfirmButton: false
                });
            }
        })
    }
});

$('#btnSignup').click(function (e) {
    e.preventDefault();
    //Inputs Mapper
    var _fullname = $('#txtfullnamesignup').val();
    var _email = $('#txtemailsignup').val();
    var _password = $('#txtpasswordsignup').val();

    //Ajax Call for Signup Users
    if (_fullname == "") {
        swal({
            title: "Error",
            text: "Please enter your full name",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_email == "") {
        swal({
            title: "Error",
            text: "Please enter your email address",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else if (_password == "") {
        swal({
            title: "Error",
            text: "Please enter your password",
            icon: "warning",
            timer: 1000,
            showConfirmButton: false
        })
    }
    else {
        //Ajax Call for Login/Signup
        $.ajax({
            type: "POST",
            url: '/LoginSignup/Signup/',
            contentType: "application/json",
            traditional: true,
            data: JSON.stringify({ fullname: _fullname, email: _email, password: _password }),
            dataType: "json",
            success: function (recData) {
                if (recData.status == "success") {

                    //Message Success
                    swal({
                        title: "Done!",
                        text: recData.data,
                        icon: "success",
                        timer: 1000,
                        showConfirmButton: false
                    });
                    location.reload(true);
                    ClearInputs();
                }
                else {
                    swal({
                        title: "Error!",
                        text: recData.data,
                        icon: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }

            },
            error: function () {
                swal({
                    title: "Error!",
                    text: recData.data,
                    icon: "error",
                    timer: 1000,
                    showConfirmButton: false
                });
            }
        })
    }
});
function ClearInputs() {
    $('#txtfullname').val('');
    $('#txtemail').val('');
    $('#txtpassword').val('');
    $('#txtfullnamesignup').val('');
    $('#txtemailsignup').val('');
    $('#txtpasswordsignup').val('');
}