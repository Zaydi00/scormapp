﻿$(document).ready(function () {
    ClearInputs();
})
//Button Click
$('#btnSend').click(function (e) {
    e.preventDefault();
    //Inputs Mapper
    var _fullname = $('#txtname').val();
    var _email = $('#txtemail').val();
    var _message = $('#txtmessage').val();

    //Ajax Call for Sending Mail
    if (_fullname == "") {
        swal({
            title: "Error",
            text: "Please enter your name",
            icon: "warning",
        })
    }
    else if (_email == "") {
        swal({
            title: "Error",
            text: "Please enter your email address",
            icon: "warning",
        })
    }
    else if (_message == "") {
        swal({
            title: "Error",
            text: "Please enter message",
            icon: "warning",
        })
    }
    else {
            //Ajax Call for Sending Mail
            $.ajax({
                type: "POST",
                url: '/SendingEmails/SendMail',
                contentType: "application/json",
                traditional: true,
                data: JSON.stringify({ fullname: _fullname, email: _email, message:_message }),
                dataType: "json",
                success: function (recData) {
                    if (recData.status == "success") {

                        //Message Sended
                        swal({
                            title: "Done!",
                            text: recData.data,
                            icon: "success",
                        });
                        ClearInputs();
                    }
                    else {
                        swal({
                            title: "Error!",
                            text: recData.data,
                            icon: "error",
                        });
                    }

                },
                error: function () {
                    swal({
                        title: "Error!",
                        text: recData.data,
                        icon: "error",
                    });
                }
            })
         }
});
function ClearInputs() {
    $('#txtname').val('');
    $('#txtemail').val('');
    $('#txtmessage').val('');
}