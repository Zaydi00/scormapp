﻿using Microsoft.Office.Core;
using Microsoft.Office.Interop.PowerPoint;
using scormpk.Controllers.Settings;
using scormpk.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Spire.Presentation;
using System.Drawing.Imaging;

namespace scormpk.Controllers.Files
{
    public class FilesController : Controller
    {
        [HttpPost]
        public ActionResult UploadFiles(HttpPostedFileBase file)
        {
            try
            {
                using (scormpkcon db = new scormpkcon())
                {
                    var filesize = 2097152;
                    var supportedTypes = new[] { "ppt", "pptx" };
                    var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                    if (!supportedTypes.Contains(fileExt))
                    {
                        TempData["Errormsg"] = "File Extension Is InValid - Only Upload PPT / PPTX File";
                        return RedirectToAction("Index", "Scorm");
                    }
                    else if (file.ContentLength > filesize)
                    {
                        TempData["Errormsg"] = "File size Should Be UpTo 2 MB";
                        return RedirectToAction("Index", "Scorm");
                    }
                    else
                    {
                        //Uploading File
                        string filename = Guid.NewGuid() + Path.GetExtension(file.FileName);
                        file.SaveAs(Path.Combine(Server.MapPath("~/App_Data/UploadedFiles/"), filename));
                        var path = Server.MapPath(@"~/App_Data/UploadedFiles/");
                        //var path = HttpContext.Current.Server.MapPath(@"~/App_Data/UploadedFiles/");
                        //Conversion

                        Spire.Presentation.Presentation presentation = new Spire.Presentation.Presentation();

                        presentation.LoadFromFile(path + filename);

                        //traverse the slides of PPT files
                        for (int i = 0; i < presentation.Slides.Count; i++)
                        {
                            //save the slide to Image
                            Image image = presentation.Slides[i].SaveAsImage();

                            String fileName = String.Format("result-img-{0}.png", i);
                            image.Save(filename, ImageFormat.Jpeg);
                            //view the image
                            System.Diagnostics.Process.Start(fileName);
                        }

                        //Saving Database
                        tblFile f = new tblFile();
                        f.FileID = ProgramSettings.assignId("tblFiles", "FileID", "");
                        f.FileName = file.FileName;
                        f.FileGUID = filename;
                        db.tblFiles.Add(f);
                        db.SaveChanges();
                        TempData["Errormsg"] = "File has been successfully uploaded";
                        return RedirectToAction("Index", "Scorm");
                        //return Json(new { status = "success", data = "File Uploaded Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception x)
            {
                TempData["Errormsg"] = x.Message;
                return RedirectToAction("Index", "Scorm");
            }
        }
        //[HttpPost]
        //public ActionResult UploadFiles(HttpPostedFileBase file)
        //{
        //    string guid = Guid.NewGuid().ToString();


        //    string msg = "Something went wrong, Your file was not uploaded.";
        //    string FinalPath = "";
        //    if (file != null && file.ContentLength < 2097152)
        //    {
        //        var fileName = Path.GetFileName(file.FileName);             // extract only the filename
        //        var ext = Path.GetExtension(fileName.ToLower());  
        //        if (ext == ".ppt" || ext == ".pptx")
        //        {
        //            Microsoft.Office.Interop.PowerPoint.Presentation pptPresentation = pptApplication.Presentations.Open2007(fileName);
        //            foreach (Microsoft.Office.Interop.PowerPoint.Slide pptSlide in pptPresentation.Slides)
        //            {
        //                pptSlide.Export("SlidesTest", "PNG", 1024, 768);
        //            }
        //        }
        //    }
        //    return View();
        //}
    }
}