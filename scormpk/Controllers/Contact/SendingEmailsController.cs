﻿using scormpk.Controllers.Settings;
using scormpk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace scormpk.Controllers.Contact
{
    public class SendingEmailsController : Controller
    {
        //For Testing only, otherwise replace the email
        protected string _fromEmail = "syedmohammadhumzazaydi@gmail.com";
        [HttpPost]
        public ActionResult SendMail(string fullname, string email, string message)
        {
            try
            {
                using (scormpkcon db = new scormpkcon())
                {
                    if (fullname !=null && email !=null && message !=null)
                    {
                        //Saving User Data
                        tblMessage msg = new tblMessage();
                        msg.MessageID = ProgramSettings.assignId("tblMessages", "MessageID", "");
                        msg.Fullname = fullname;
                        msg.EmailAddress = email;
                        msg.Message = message;
                        db.tblMessages.Add(msg);
                        db.SaveChanges();

                        //Sending Mail
                        MailMessage mailMessage = new MailMessage(email, _fromEmail);
                        mailMessage.Subject = fullname;
                        mailMessage.Body = message;
                        SmtpClient client = new SmtpClient();
                        client.Send(mailMessage);
                        return Json(new { status = "success", data = "Mail successfully sended" },JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = "error", data = "Please Fill all fields" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception x)
            {
                return Json(new { status = "error", data = x.Message }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}