﻿using scormpk.Controllers.Settings;
using scormpk.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace scormpk.Controllers.Users
{
    public class LoginSignupController : Controller
    {
        [HttpPost]
        public ActionResult Signup(string fullname, string email, string password)
        {
            try
            {
                using (scormpkcon db = new scormpkcon())
                {
                    tblUser user = new tblUser();
                    user.UserID = ProgramSettings.assignId("tblUsers", "UserID", "");
                    user.FullName = fullname;
                    user.Email = email;
                    user.Password = password;
                    user.CreatedDate = DateTime.Now;
                    db.tblUsers.Add(user);
                    db.SaveChanges();
                    return Json(new { status = "success", data = "Successfully Signed Up" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception x)
            {
                return Json(new { status = "error", data = x.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            try
            {
                using (scormpkcon db = new scormpkcon())
                {
                    var chk = (from x in db.tblUsers
                               where x.Email == email
                               && x.Password == password
                               select x).SingleOrDefault();
                    if (chk != null)
                    {
                        Session["UserID"] = chk.UserID;
                        Session["UserName"] = chk.FullName;
                        return Json(new { status = "success", data = "Successfully Logged In" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { status = "error", data = "Invalid email & password!" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception x)
            {
                return Json(new { status = "error", data = x.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Index", "Scorm");
        }
    }
}